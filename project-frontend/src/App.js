import "./App.css";
import Form from "react-bootstrap/Form";
import { Button, Container, Row, Col } from "reactstrap";
import axios from "axios";
import base_url from "./api/bootapi";
import { useState } from "react";
import { ToastContainer, toast } from "react-toastify";

import Header from "./components/header/Header";
import Menu from "./components/Menu";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  path,
  component,
  Link,
  exact,
  NavLink,
} from "react-router-dom";

import GroupDetails from "./pages/GroupDetails";

function getSessionStorageOrDefault(key, defaultValue) {
  console.log("default");
  console.log(defaultValue);
  console.log("stored");
  const stored = sessionStorage.getItem(key);
  console.log(stored);
  if (!stored) {
    console.log("!stored " + !stored);
    return defaultValue;
  }
  return JSON.parse(stored);
}

function App() {
  console.log("inside app");
  const [isActive, setIsActive] = useState(
    getSessionStorageOrDefault("status", false)
  );

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  //login
  const signinUser = () => {
    console.log("signinuse called");
    if (email.length == 0) {
      toast.warning("please enter email");
    } else if (password.length == 0) {
      toast.warning("please enter password");
    } else {
      const body = {
        email,
        password,
      };

      // url to make signin api call
      const url = `${base_url}/signin`;

      // make api call using axios
      axios.post(url, body).then((response) => {
        // get the server result
        const result = response.data;
        console.log(result);
        if (result["status"] == "success") {
          toast.success("Welcome to the application");

          // get the data sent by server
          const { id, firstName, lastName } = result["data"];

          // persist the logged in user's information for future use
          sessionStorage["id"] = id;
          sessionStorage["firstName"] = firstName;
          sessionStorage["lastName"] = lastName;
          sessionStorage["status"] = true;
          {
            setIsActive(1);
          }
        } else {
          toast.error("Invalid user name or password");
        }
      });
    }
  };

  function handleSubmit(event) {
    event.preventDefault();
  }

  //if false return login page
  if (!isActive) {
    return (
      <div className="container">
        <Router>
          <div className="Login">
            <Form onSubmit={handleSubmit}>
              <h3
                style={{
                  textAlign: "center",
                  color: "blueviolet",
                  fontFamily: "cursive",
                  paddingBottom: "20px",
                  fontSize: "30px",

                  marginBottom: "20px",
                }}
              >
                MeetBuddy
              </h3>
              <Form.Group size="lg" controlId="email">
                <Form.Label style={{ color: "gray" }}>Email</Form.Label>

                <Form.Control
                  autoFocus
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Form.Group>
              <Form.Group size="lg" controlId="password">
                <Form.Label style={{ color: "gray" }}>Password</Form.Label>

                <Form.Control
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>

              <div style={{ color: "gray" }}>
                No account yet?<br></br> <Link to="/signup">Signup here</Link>
              </div>

              <br></br>

              <Button
                onClick={signinUser}
                size="lg"
                type="submit"

                // disabled={!validateForm()}
              >
                Login
              </Button>
            </Form>
            <ToastContainer theme="colored" />
          </div>
        </Router>
      </div>
    );
  }
  //if condition true return home page
  return (
    <div>
      {console.log("isactive home " + isActive)}
      <div className="container">
        <div>
          <Router>
            <div>
              <Header></Header>
            </div>

            <Row>
              <Col
                md={2}
                style={{
                  border: "solid 1px",
                  color: "gray",
                  backgroundColor: "black",
                }}
              >
                <h3
                  style={{
                    textAlign: "center",
                    color: "blueviolet",
                    fontFamily: "cursive",
                  }}
                >
                  Meet Buddy
                </h3>
                <Menu></Menu>
              </Col>

              <Col md={10}>
                <Routes>
                  <Route
                    exact
                    path="/groupdetails"
                    element={<GroupDetails></GroupDetails>}
                  />

                  {/* <Route exact path="/profile" element={<Profile></Profile>} />

                
                  <Route exact path="/friends" element={<Friends></Friends>} /> */}


                  
                </Routes>
              </Col>
            </Row>
          </Router>
        </div>
      </div>
      <ToastContainer theme="colored" />
    </div>
  );
}

export default App;
