import './index.css'
import {Link} from 'react-router-dom'
import {useState} from 'react'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import base_url from  '../../api/bootapi'

const CreateGroup =()=>{
    const [groupName ,setGroupName]=useState('')
    const [groupDesc ,setGroupDesc]=useState('')
    const [groupId ,setGroupId]=useState()
    const navigate = useNavigate()
    const createGroup =()=>{
        const currentuser_id=sessionStorage['id'];
     //  console.log( sessionStorage.getItem(id));
     //  console.log(id);
        const body={
            groupName,
            groupDesc,
            userIdFk:currentuser_id,//for storing value of user id into group table
        }
    const url = `${base_url}/groups/${currentuser_id}`
    axios.post(url, body).then((response) => {
        // get the server result
      const  result=( response.data)
        console.log(response)
       // if (response.status == 'success') {
       //     toast.success('Welcome to the application')
        //    navigate('/groupdetails')

        const{groupId,groupName}=result
        sessionStorage['setGroupName']=groupName
        sessionStorage['setGroupId']=groupId
    // } else {
    //     toast.error('group not created')
    //   }
    })
}
    return(
        <div style={{backgroundColor:"lightgray",margin:"50px",padding:"auto",border:"solid"}}>
        <h1  style={{
          textAlign: "center",
          fontSize: "50px",
          fontFamily: "cursive",
          color:"blue"
        }}>Create group </h1>
        <ul>
      
     </ul>
        <div className="row">
        <div className="col"></div>
        <div className="col">
        <div className="form">
         <div className="mb-3">
            <label htmlFor="" className="label-control">
                Group Name
            </label> 
            <input onChange={(e)=>{
             setGroupName(e.target.value)   
            }} type="text" className="form-control"/>
         </div>
         <div className="mb-3">
            <label htmlFor="" className="label-control">
                Group Description
            </label> 
            <input onChange={(e)=>{
             setGroupDesc(e.target.value)   
            }}  type="text" className="form-control"/>
        </div>
            <button onClick={createGroup} className="btn btn-success">Submit</button>
        </div> 
        </div>
        <div className="col"></div>  
        </div>
    </div>
    )
}

export default CreateGroup