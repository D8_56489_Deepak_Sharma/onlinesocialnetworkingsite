import { useNavigate } from 'react-router'
import Post from '../../components/post'
import SearchBar from '../../components/searchBar'
import { useEffect, useState } from 'react'

import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'

const SearchPost = () => {
  
  const navigate = useNavigate()

  
  
  // load the data in the beginning
  useEffect(() => {
    search()
    console.log('getting called')
  }, [])

  const logoutUser = () => {
    // remove the logged users details from session storage
    sessionStorage.removeItem('id')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')
    sessionStorage.removeItem('loginStatus')

    // navigate to sign in component
    navigate('/signin')
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <h1>Posts</h1>
        </div>

        <div className="col">
          <div className="float-end">
            <div className="btn-group " role="group">
              <button
                id="btnGroupDrop1"
                type="button"
                className="btn btn-primary dropdown-toggle"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Welcome 
              </button>
              <ul className="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <li>
                  <Link to="/create-post" className="dropdown-item">
                    Create
                  </Link>
                </li>
                <li>
                  <a className="dropdown-item">Profile</a>
                </li>
                <li>
                  <button onClick={logoutUser} className="dropdown-item">
                    Logout
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      

      <div className="row" style={{ marginTop: '20px' }}>
        <div className="col">
          <SearchBar />
        </div>
      </div>

      <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
        <div className="col">
          {posts.map((post) => {
            return <Post post={post} />
          })}
        </div>
      </div>
    </div>
  )
}

export default SearchPost
