import { useEffect, useState } from "react";
import axios from "axios";
import "./index.css";
import { toast } from "react-toastify";
import FileDetails from "../FileDetails/index"
import File from "../File/index"

import URL from "../../api/bootapi";
import { useNavigate } from "react-router";
const styles = {
  detailsContainer: {
    marginTop: "10px",
    marginBottom: "10px",
  },
};

const GroupDetails = () => {
  const navigate = useNavigate();
  const [groups, setGroups] = useState([]);
  

  const getGroups = () => {
    const url = `${URL}/groups`;
    axios.get(url).then((response) => {
      // const result = response.data
      setGroups(response.data);

      console.log(response);
    });
  };
  useEffect(() => {
    getGroups();
    console.log("getting called");
  }, []);

  return (

    <div className="container">
      <h1>Group Details </h1>

      {groups.map((group) => (
        <div key={group.groupId}>
          <div class="row">
            <div className="group">groupName:{group.groupName}</div>

            <div className="img">
              {group.files.map((file) => {
                return (
                  <ul key={group.groupId}>
                    <li>
                      data: <img src={`data:image/jpeg;base64,${file.data}`} />
                    </li>
                  </ul>
                );
              })}
            </div>
            
          </div>
        </div>
      ))}
    </div>
  );
};
export default GroupDetails;
