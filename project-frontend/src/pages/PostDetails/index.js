import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router'
import Post from '../../components/post'
import axios from 'axios'
import { URL } from '../../config'
import Comment from '../../components/comment'
import { toast } from 'react-toastify'

const PostDetails = (props) => {
  const { state } = useLocation()
  const [comment, setComment] = useState('')
  const [post, setPost] = useState()
  const currentUserId = sessionStorage['id']
  const navigate = useNavigate()

  const loadPostDetails = () => {
    const { id } = state
    const url = `${URL}/post/details/${id}`
    axios.get(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        setPost(result['data'])
      }
    })
  }

  useEffect(() => {
    loadPostDetails()
  }, [])

  const sendComment = () => {
    if (comment.length == 0) {
      toast.warning('please enter comment')
    } else {
      const { id } = state

      const body = {
        userId: currentUserId,
        comment,
      }

      const url = `${URL}/comment/${id}`
      axios.post(url, body).then((response) => {
        const result = response.data

        // clear the comment box
        setComment('')

        if (result['status'] == 'success') {
          toast.success('successfully added a comment')
         

          // refreshing the post details
          loadPostDetails()
        } else {
          toast.error(result['error'])
        }
      })
    }
  }

  const deletepost = () => {
    const { id } = state
  

      const url = `${URL}/post/${id}`
      axios.delete(url).then((response) => {
        const result = response.data
        if (result['status'] == 'success') {
          toast.success('successfully deleted post..')
          navigate('/home')
        } else {
          toast.error(result['error'])
        }
      })
    }
  


  return (
    <div>
      <div className="row">
        <div className="col">
          <h1 className="title">Post Details</h1>
        </div>
      </div>  
      <div className="row">
        <div className="col">
          {post && post.userId == currentUserId && (
            <button
              onClick={() => {
                navigate('/edit-post', { state: { post: post } })
              }}
              className="btn btn-success float-end"
            >
              Edit
            </button>
          )}
        </div>
        </div>
          
        <div className="row">
        <div className="col">
          {post && post.userId == currentUserId && (
            <button
              onClick={deletepost}
              className="btn btn-danger float-end"
            >
              Delete
            </button>
          )}
        </div>
        </div>

        
      

      {post && (
        <div>
          <div className="row">
            <div className="col">
              <Post post={post} isDetails={true} />
            </div>
          </div>
          <hr />

          <div className="row">
            <div className="col">
              {post.comments.map((comment) => {
                return <Comment comment={comment} />
              })}
            </div>
          </div>
        </div>
      )}

      {post && currentUserId != post.userId && (
        <div>
          <div className="row">
            <div className="col">
              <textarea
                onChange={(e) => {
                  setComment(e.target.value)
                }}
                rows="2"
                className="form-control"
                placeholder="your comment here"
              ></textarea>
            </div>
          </div>

          <div className="row">
            <div className="col">
              <button
                onClick={sendComment}
                style={{ marginTop: '10px' }}
                className="btn btn-success"
              >
                Send
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default PostDetails
