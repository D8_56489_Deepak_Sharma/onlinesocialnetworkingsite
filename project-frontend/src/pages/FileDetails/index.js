import React from "react";

import { useEffect, useState } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import base_url from "../../api/bootapi";

const styles = {
  detailsContainer: {
    marginTop: "10px",
    marginBottom: "10px",
    
  },
  imgstyles: {
    width: "250px",
  },
};

export const FileDetails = () => {
  const [file, setFile] = useState([]);
  const getFiles = () => {
    const url = `${base_url}/files`;
    axios.get(url).then((response) => {
      // const result = response.data
      setFile(response.data);

      console.log(response);
    });
  };
  useEffect(() => {
    getFiles();
    console.log("getting called");
  }, []);

  return (
    <div className="row">
      <div className="col">
        <h1>File Details</h1>

        {file.map((file) => (
          <div className="row" style={styles.detailsContainer} key={file.id}>
            file: {file.fileId}
            <br></br>
            name: {file.name}
            <br></br>
            data: {file.data}
            <img
              style={styles.imgstyles}
              className="img"
              src={`data:image/jpeg;base64,${file.data}`}
            />
          </div>
        ))}
      </div>
    </div>
  );
};
export default FileDetails;
