import axios from 'axios'
import { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import base_url from  '../../api/bootapi'

const CreatePost = () => {
  const [title, setTitle] = useState('')
  const [details, setDetails] = useState('')
  const [dataFile, setDataFile] = useState('')

  const navigate = useNavigate()
 


  const save = () => {
    if (title.length == 0) {
      toast.warning('please enter title')
    }  else if (details.length == 0) {
      toast.warning('please enter contents')
    } else {
      const body = {
        title,
        details,
        dataFile,
        userId: sessionStorage['id'],
      }


      const formData = new FormData()
      formData.append('title',title)
      formData.append('details',details)
      formData.append('dataFile',dataFile)
      formData.append('userId',sessionStorage['id'])
      console.log(formData)

      const url = `${URL}/post/`
      axios.post(url, formData).then((response) => {
        const result = response.data
        if (result['status'] == 'success') {
          toast.success('added new post..')
          navigate('/home')
        } else {
          toast.error(result['error'])
        }
      })
    }
  }

  return (
    <div>
      <h1 className="title">Create Post</h1>

      <div className="form">
        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Title
          </label>
          <input
            onChange={(e) => {
              setTitle(e.target.value)
            }}
            type="text"
            className="form-control"
          />
        </div>

        <div className="mb-3">
            <label for="formFile" className="form-label">Default file input example</label>
            <input 
            onChange={(e) => {
              setDataFile(e.target.files[0])
              console.log(e.target.files)
            }}
            className="form-control" type="file" ></input>
        </div>

        

        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Details
          </label>
          <textarea
            onChange={(e) => {
              setDetails(e.target.value)
            }}
            rows="10"
            className="form-control"
          ></textarea>
        </div>

        <div className="mb-3">
          <button onClick={save} className="btn btn-success">
            Save
          </button>
          <Link to="/home" className="btn btn-danger float-end">
            Cancel
          </Link>
        </div>
      </div>
    </div>
  )
}

export default CreatePost
