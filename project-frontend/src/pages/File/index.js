import React from 'react';
import {useState} from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router'
import base_url from '../../api/bootapi';
const File = () => {
  const [data ,setData]=useState(null);
  const [add ,setAddData]=useState(null);
  const[groupSocial_groupId,setGroupSocial_groupId]=useState();
  const[FileId,setFileId]=useState();
  const navigate = useNavigate()

  const createData =()=>{
    const formData = new FormData();
    formData.append("file", data);
  
  const url = `${base_url}/upload`
  axios.post(url, formData).then((response) => {
    // get the server result
    setData( response.data)
    console.log(response)
    const{FileId,groupId}=data
        sessionStorage['setGroupSocial_groupId']=groupSocial_groupId
        sessionStorage['setFileId']=FileId
   
  })
}
  const addData =()=>{
    const{id,fileId}=sessionStorage
    const formData = new FormData();
    formData.append("file", data);
  const url = `${URL}/${groupSocial_groupId}/file/${fileId}/files`
  axios.post(url, formData).then((response) => {
    // get the server result
    setAddData( response.data)
    console.log(response)
   
  })

  navigate('/filedetails')
  }
 
return(
  <div className="File" >
    <form>
      <div >
        <label> Select File</label>
        <input type="file" 
          onChange={(e) => setData(e.target.files[0])}  />
      </div>
      <button onClick={createData}>Upload</button>
      <button onClick={addData}>Add File to Group</button>
    </form>
  </div>
)
     
}
 

export default File