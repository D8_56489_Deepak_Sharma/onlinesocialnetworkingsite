import React from "react";
import { Link } from "react-router-dom";
import { ListGroup } from "reactstrap";

export default class Menu extends React.Component {
  render() {
    return (
      <ListGroup
        flush
        style={{
          textAlign: "center",
          fontSize: "20px",
          fontFamily: "cursive",
          
        }}
      >
        <Link
          className="list-group-item list-group-item-action"
          tag="a"
          to="/home"
        >
          Home
        </Link>

        <Link
          className="list-group-item list-group-item-action"
          tag="a"
          to="/profile"
        >
          Profile
        </Link>

        <Link
          className="list-group-item list-group-item-action"
          tag="a"
          to="/post"
        >
          Post
        </Link>

        <Link
          className="list-group-item list-group-item-action"
          tag="a"
          to="/friends"
        >
          Friends
        </Link>

        <Link
          className="list-group-item list-group-item-action"
          tag="a"
          to="/groupdetails"
        >
          Groups Details
        </Link>

        {/* <Link className="list-group-item list-group-item-action" 
        tag="a" 
        to="">
          Contact
        </Link> */}
      </ListGroup>
    );
  }
}
