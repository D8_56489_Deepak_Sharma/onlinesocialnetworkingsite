import { Row ,Col } from "reactstrap";
import "./header.css"
import { useNavigate } from "react-router";
function Header() {


  const logoutUser = () => {
    // remove the logged users details from session storage
    sessionStorage.removeItem("id");
    sessionStorage.removeItem("firstName");
    sessionStorage.removeItem("lastName");
    sessionStorage.removeItem("status");

    // navigate to sign in component
window.location.reload(false);
  };

  return (
    <div>
      <Row className="header">
        <Col style={{fontFamily:"cursive",color:"darkgoldenrod"}} md={2} className="user">{sessionStorage.getItem("firstName")}</Col>
        <Col md={8}>
          <h1 style={{ fontFamily: "cursive", color: "darkcyan" }}>
            <div className="home">
              <div class="input-group">
                <input
                  type="search"
                  class="form-control rounded"
                  placeholder="Search"
                  aria-label="Search"
                  aria-describedby="search-addon"
                />
                <span>&nbsp;&nbsp;</span>
                <button
                  style={{ backgroundColor: "black " ,width:"100px"}}
                  type="button"
                  class="btn btn-outline-primary"
                >
                  search
                </button>
              </div>
            </div>
          </h1>
        </Col>
        <Col>
          <h1 md={2} style={{ fontFamily: "cursive", color: "darkcyan" }}>
            <p>
              <a onClick={logoutUser} class="btn btn-info btn-lg">
                <span class="glyphicon glyphicon-log-out"></span> Log out
              </a>
            </p>
          </h1>
        </Col>
      </Row>
    </div>
  );
}

export default Header;

