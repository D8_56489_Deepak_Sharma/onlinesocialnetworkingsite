package com.socialsite.dtos;

import com.socialsite.entities.User;

public class FriendShipDto {
	private int id;
	private User user;
	private User friend;
	private int status;

	public FriendShipDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FriendShipDto(int id, User user, User friend, int status) {
		super();
		this.id = id;
		this.user = user;
		this.friend = friend;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getFriend() {
		return friend;
	}

	public void setFriend(User friend) {
		this.friend = friend;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
