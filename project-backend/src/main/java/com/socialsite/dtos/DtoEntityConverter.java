package com.socialsite.dtos;

import org.springframework.stereotype.Component;

import com.socialsite.entities.FriendShip;
import com.socialsite.entities.User;

@Component
public class DtoEntityConverter {
	public UserDto toUserDto(User entity) {
		UserDto userDto = new UserDto();
		userDto.setId(entity.getId());
		userDto.setFirstName(entity.getFirstName());
		userDto.setLastName(entity.getLastName());
		userDto.setEmail(entity.getEmail());
		userDto.setPassword(entity.getPassword());
		return userDto;

	}
	
	public User toUserEntity(UserDto dto) {
		User entity = new User();
		entity.setId(dto.getId());
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setEmail(dto.getEmail());
		entity.setPassword(dto.getPassword());
		return entity;

	}
	
	public FriendShip toFriendShipEntity(FriendShipDto dto) {
		FriendShip friendShip = new FriendShip();
		friendShip.setUser(dto.getUser());
		friendShip.setFriend(dto.getFriend());
		friendShip.setStatus(dto.getStatus());
		
		return friendShip;
	}
}
