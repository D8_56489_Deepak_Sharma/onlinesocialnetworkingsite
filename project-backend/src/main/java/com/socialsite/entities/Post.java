package com.socialsite.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "post")
public class Post {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "userId")
	private User user;
	private String title;
	private String details;

	@Temporal(TemporalType.TIMESTAMP)

	private Date createdTimestamp = new Date();

	@Lob
	private byte[] data;

	public Post() {
	}

	public Post(int id, String title, String details, Date createdTimestamp, byte[] data) {
		this.id = id;

		this.title = title;
		this.details = details;
		this.createdTimestamp = createdTimestamp;
		this.data = data;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Post(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

//	
	@Override
	public String toString() {
		return String.format("Post [id=%s, user=%s, title=%s, details=%s, , createdTimestamp=%s, data=%s]", id,
				(user == null ? "-" : user.getId()), title, details, createdTimestamp,
				(data == null ? 0 : data.length));
	}

}
