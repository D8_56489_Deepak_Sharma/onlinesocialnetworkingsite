package com.socialsite.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class FriendShip {
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@OneToOne
	private User user;
	@OneToOne
	private User friend;
	private int status = 0;

	public FriendShip() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public FriendShip(int id, User user, User friend, int status) {
		super();
		this.id = id;
		this.user = user;
		this.friend = friend;
		this.status = status;
	}



	public FriendShip(User user, User friend, int status) {
		super();
		this.user = user;
		this.friend = friend;
		this.status = status;
	}

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getFriend() {
		return friend;
	}

	public void setFriend(User friend) {
		this.friend = friend;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
