package com.socialsite.controllers;

import java.util.ArrayList;
import java.util.List;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

//import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.socialsite.entities.Post;
import com.socialsite.services.PostServiceImpl;

@CrossOrigin(origins = "*")
@RestController
public class PostControllerImpl {
	@Autowired
	private PostServiceImpl postService;
	
	@GetMapping("/post/search")
	public ResponseEntity<?> findPosts(
			@RequestParam(name = "title", defaultValue = "") String title,
			HttpServletResponse resp) {
		List<Post> result = new ArrayList<>();
		if(!title.isEmpty())
			result = postService.findPostsByTitle(title);
		else
			result = postService.findAllPosts();
		return Response.success(result);
	}
	
	@GetMapping("/getpost/details/{id}")
	public ResponseEntity<?> findGetPostById(@PathVariable("id") int id) {
		Post res = postService.findGetPostById(id);
		return Response.success(res);
	}
	
	@GetMapping(value = "/image/{postId}", produces = "image/jpeg")
	public @ResponseBody byte[] downloadAttachment3(@PathVariable("postId") int blogId) {
		// get from file --> Java File IO --> byte[] --> return
		Post attachment = postService.findDataByPostId(blogId);
		if(attachment == null)
			return null;
		return attachment.getData();
	}
	
	
	@PostMapping("/addpost")
	public ResponseEntity<?> addPost(@RequestParam int userId, @RequestParam String title,@RequestParam String details, MultipartFile dataFile ) {
		System.out.println("Inserting: " );
		Map<String, Object> result = postService.addingPost(userId,title,details,dataFile);
		return Response.success(result);
	}
	
	@PutMapping("/editPost/{id}")
	public ResponseEntity<?> editPost(@PathVariable("id") int id,  @RequestParam String title,@RequestParam String details, 
			MultipartFile dataFile ) {
		Map<String, Object> result = postService.editPost(id,title,details,dataFile );
		return Response.success(result);
	}

	
	
	@DeleteMapping("/post/{id}")
	public ResponseEntity<?> deletePost(@PathVariable("id") int id) {
		Map<String, Object> result = postService.deletePost(id);
		return Response.success(result);
	}
	
	
}
