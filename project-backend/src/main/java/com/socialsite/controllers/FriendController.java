package com.socialsite.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.socialsite.dtos.UserDto;
import com.socialsite.services.FriendShipServiceImpl;

@RestController
@CrossOrigin
public class FriendController {

	@Autowired
	private FriendShipServiceImpl friendShipService;

	// friend request
	@GetMapping("/request/{user_id}/{friend_id}")
	public ResponseEntity<?> friendRequest(@PathVariable("user_id") int user_id,
			@PathVariable("friend_id") int friend_id) {

		try {
			if (user_id == friend_id)
				return Response.error("Request cannot be sent to same person");
			Map<String, Object> result = friendShipService.friendRequest(user_id, friend_id);
			if (result == null)
				return Response.error("Request Not Sent");
			return Response.success(result);

		} catch (Exception e) {
			return Response.error(e.getMessage());
		}

	}

	// request received by an individual
	@GetMapping("/request/received/{user_id}")
	public ResponseEntity<List<UserDto>> requestReceived(@PathVariable("user_id") int user_id) {
		List<UserDto> result = friendShipService.requestReceived(user_id);
		return new ResponseEntity<List<UserDto>>(result, HttpStatus.OK);
	}
	

	// accepting request
	@GetMapping("/request/accept/{user_id}/{friend_id}")
	public ResponseEntity<?>  requestAccept(@PathVariable("user_id") int user_id,
			@PathVariable("friend_id") int friend_id) {
		Map<String, Object> result= friendShipService.acceptFriendRequest(user_id, friend_id);
		if (result == null)
			return Response.error(result);
		return Response.success(result);
		
	}
	
	// friend list
	@GetMapping("/friendlist/{user_id}")
	public ResponseEntity<List<UserDto>> friendList(@PathVariable("user_id") int user_id) {
		List<UserDto> result = friendShipService.getFriendListByUserId(user_id);
		return new ResponseEntity<List<UserDto>>(result, HttpStatus.OK);
	}
}
