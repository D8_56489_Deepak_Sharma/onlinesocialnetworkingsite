package com.socialsite.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialsite.dtos.Credentials;
import com.socialsite.dtos.UserDto;
import com.socialsite.entities.User;
import com.socialsite.services.UserServiceImpl;

@RestController
@CrossOrigin
public class UserControllerImpl {
	
	@Autowired
	UserServiceImpl userServiceImpl;

	// signup
	@PostMapping("/signup")
	public ResponseEntity<?> signUp(@RequestBody User user) {
		try {
			Map<String, Object> result = userServiceImpl.saveUser(user);
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	

	@GetMapping("/profile/{id}")
	public ResponseEntity<?> getProfile(@PathVariable("id") int id) {
		try {
			User result = userServiceImpl.findUserById(id);
			if(result==null)
		 Response.error("user not found");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	
	
	// signin
	@PostMapping("/signin")
	public ResponseEntity<?> signIn(@RequestBody Credentials cred) {
		try {
			UserDto result = userServiceImpl.findUserByEmailAndPassword(cred);
			if (result == null)
				return Response.error("user not found");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	// upadate user
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updateUser(@PathVariable("id") int id, @RequestBody User user) {
		try {
			user.setId(id);
			Map<String, Object> result = userServiceImpl.saveUser(user);
			if (result == null)
				return Response.status(HttpStatus.NOT_MODIFIED);
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	

	// remove user
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") int id) {
		try {
			int record = userServiceImpl.deleteUserById(id);
			if (record == 0)
				return Response.status(HttpStatus.NOT_FOUND);
			else
				return Response.success("Record Deleted");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	


}
