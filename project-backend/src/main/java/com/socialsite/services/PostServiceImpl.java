package com.socialsite.services;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import com.socialsite.daos.PostDao;
import com.socialsite.entities.Post;
import com.socialsite.entities.User;

@Transactional
@Service
public class PostServiceImpl {

	@Autowired
	private PostDao postDao;

	public List<Post> findAllPosts() {
		List<Post> postList = postDao.findAll();
		return postList.stream().collect(Collectors.toList());
	}

	public List<Post> findPostsByTitle(String title) {
		List<Post> postList = postDao.findByTitleContaining(title);
		return postList.stream().collect(Collectors.toList());
	}

	public Post findGetPostById(int blogId) {
		Post post = postDao.findById(blogId);
		return post;
	}

	public Map<String, Object> editPost(int postId, String title, String details, MultipartFile dataFile) {
		if (postDao.existsById(postId)) {
			Post b = new Post();
			b.setId(postId);

			b.setTitle(title);
			b.setDetails(details);
			try {
				b.setData(dataFile.getBytes());
			} catch (Exception e) {
				throw new MultipartException("Can't convert MultipartFile to bytes : " + b.getData(), e);
			}
			b = postDao.save(b);

			return Collections.singletonMap("changedRows", 1);
		}
		return Collections.singletonMap("changedRows", 0);
	}

	public Post findDataByPostId(int postId) {
		return postDao.findById(postId);
	}

	public Map<String, Object> addingPost(int userId, String title, String details, MultipartFile dataFile) {
		Post b = new Post();

		User user = new User();
		user.setId(userId);
		b.setTitle(title);
		b.setDetails(details);
		try {
			b.setData(dataFile.getBytes());
		} catch (Exception e) {
			throw new MultipartException("Can't convert MultipartFile to bytes : " + b.getData(), e);
		}
		b = postDao.save(b);
		return Collections.singletonMap("insertedId", b.getId());

	}

	public Map<String, Object> deletePost(int postId) {
		if (postDao.existsById(postId)) {
			postDao.deleteById(postId);
			return Collections.singletonMap("affectedRows", 1);
		}
		return Collections.singletonMap("affectedRows", 0);
	}

}
