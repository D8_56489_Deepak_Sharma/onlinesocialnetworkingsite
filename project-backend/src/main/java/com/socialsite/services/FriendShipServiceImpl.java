package com.socialsite.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialsite.daos.FriendShipDao;
import com.socialsite.daos.UserDao;
import com.socialsite.dtos.DtoEntityConverter;
import com.socialsite.dtos.FriendShipDto;
import com.socialsite.dtos.UserDto;
import com.socialsite.entities.FriendShip;

@Transactional
@Service
public class FriendShipServiceImpl {

	@Autowired
	private FriendShipDao friendShipDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private DtoEntityConverter converter;

	// friend Request sent

	public Map<String, Object> friendRequest(int user_id, int friend_id) {
		FriendShipDto dto = new FriendShipDto();
		dto.setUser(userDao.findById(user_id));
		dto.setFriend(userDao.findById(friend_id));
		if (dto.getUser() == null || dto.getFriend() == null)
			return Collections.singletonMap("Request Sent unsuccessfully", "Friend not found");
		FriendShip friendShip = converter.toFriendShipEntity(dto);
		FriendShip requestSent = friendShipDao.save(friendShip);
		if (requestSent != null)
			return Collections.singletonMap("Request Sent Successfully", dto.getFriend());
		return null;
	}

	// return list of received request
	public List<UserDto> requestReceived(int user_id) {
		List<Integer> listOfUserId = friendShipDao.getRequestListbyUserId(user_id);
		List<UserDto> result = new ArrayList<UserDto>();
		for (Integer id : listOfUserId) {
			result.add(converter.toUserDto(userDao.findById(id).get()));

		}
		return result;
	}

	// friend request accepted
	public Map<String, Object> acceptFriendRequest(int user_id, int friend_ship) {
		int result= friendShipDao.acceptRequestById(user_id, friend_ship);
		if (result==0)
			return Collections.singletonMap("Request Sent unsuccessfully", "Friend not found");
		else {
			String msg="request accepted";
					return Collections.singletonMap("Request Accepted",msg);
		}
			
	}
	
	// list of friend
	public List<UserDto> getFriendListByUserId( int user_id){
		List<Integer> listOfFriendId = friendShipDao.getFriendListbyUserId(user_id);
		List<UserDto> result = new ArrayList<UserDto>();
		for (Integer id : listOfFriendId) {
			result.add(converter.toUserDto(userDao.findById(id).get()));

		}
		return result;
	}
}
