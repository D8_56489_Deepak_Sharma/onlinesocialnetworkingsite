package com.socialsite.services;

import java.util.Collections;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.socialsite.daos.UserDao;
import com.socialsite.dtos.Credentials;
import com.socialsite.dtos.DtoEntityConverter;
import com.socialsite.dtos.UserDto;
import com.socialsite.entities.User;

@Transactional
@Service
public class UserServiceImpl {
	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private DtoEntityConverter converter;

	public User findUserById(int userId) {
		User user = userDao.findById(userId);
		if (user != null)
			return user;
		return null;
	}

	public int deleteUserById(int userId) {
		if (userDao.existsById(userId)) {
			userDao.deleteById(userId);
			return 1;
		}
		return 0;
	}

	public UserDto findUserByEmail(String email) {
		User user = userDao.findByEmail(email);
		if (user != null)
			return converter.toUserDto(user);
		return null;
	}

	// signin
	public UserDto findUserByEmailAndPassword(Credentials cred) {
		User dbUser = userDao.findByEmail(cred.getEmail());
		String rawPassword = cred.getPassword();
		int passwordLength = rawPassword.length();
		boolean passwordMatch = passwordEncoder.matches(rawPassword, dbUser.getPassword());
		if (dbUser != null && passwordMatch) {
			UserDto userDto = converter.toUserDto(dbUser);
			StringBuilder sbString = new StringBuilder(passwordLength);
			for (int i = 0; i < passwordLength; i++) {
				sbString.append("*");
			}
			userDto.setPassword(sbString.toString());
			return userDto;
		}

		return null;
	}

	// signup
	public Map<String, Object> saveUser(User user) {

		String rawPassword = user.getPassword();

		String encodedPassword = passwordEncoder.encode(rawPassword);
		user.setPassword(encodedPassword);

		userDao.save(user);
		return Collections.singletonMap("valueInserted", user.getId());

	}
	
	
	
}
