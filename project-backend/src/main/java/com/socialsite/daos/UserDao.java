package com.socialsite.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.socialsite.entities.User;

public interface UserDao extends JpaRepository<User, Integer> {
	User findById(int userId);

	User findByEmail(String email);

}
