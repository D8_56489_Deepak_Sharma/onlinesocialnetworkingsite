package com.socialsite.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.socialsite.entities.FriendShip;

public interface FriendShipDao extends JpaRepository<FriendShip, Integer> {
	@Query(value = "select fd.friend_id from friendship fd where fd.user_id=:user_id", nativeQuery = true)
	List<Integer> getRequestListbyUserId(@Param("user_id") Integer user_id);

	@Modifying
	@Transactional
	@Query(value = "update friendship fd set status = 1 where fd.user_id=:user_id and fd.friend_id=:friend_id", nativeQuery = true)
	public int acceptRequestById(@Param("user_id") Integer user_id, @Param("friend_id") Integer friend_id);
	
	@Query(value = "select fd.friend_id from friendship fd where fd.user_id=:user_id and fd.status=1", nativeQuery = true)
	List<Integer> getFriendListbyUserId(@Param("user_id") Integer user_id);


}
