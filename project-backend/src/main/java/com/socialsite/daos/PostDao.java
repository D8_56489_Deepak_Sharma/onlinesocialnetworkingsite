package com.socialsite.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.socialsite.entities.Post;

public interface PostDao extends JpaRepository<Post, Integer> {
	Post findById(int id);
	List<Post> findByUserId(int id);
	List<Post> findAll();
	

	
	List<Post> findByTitleContaining(String title);
	
//	@Query("SELECT SUM(bl.type) FROM BlogLikeStatus bl WHERE bl.blog.id = ?1")
//	long getLikeCount(int id);
//	@Query("SELECT AVG(br.rating) FROM BlogRating br WHERE br.blog.id = ?1")
//	Float getAvgRating(int id);



}

